<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: feature recognition framework</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
     Quick navigation script. Use a div with "toc-panel" class having a
     nested div with "toc" id to place the navigation panel.
    -->
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/toc.js"></script>
 </head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/feature recognition
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='http://quaoar.su/blog/page/analysis-situs'>Download</a></td>
  <td class="header-menu"><a href="../features.html">Features</a></td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-featrec">Introduction to CAD feature recognition</h1>

<div class="toc-panel"><div id="toc"></div></div>

<h2 id="toc-featrec-motivation">Motivation</h2>

<p>
  <i>Form features</i> or simply <i>features</i> are groups of CAD faces or volume elements that are meaningful for a specific engineering activity or application. In most cases, <i>design</i> and <i>manufacturing</i> features are not the same things because a CAD part is not modelled (digitally) the same way it is manufactured. Regardless of its type, a feature is a carrier of domain knowledge associated with the geometry of a part.
</p>

<p>
  Although it might be possible to design a CAD part in terms of its ultimate manufacturing features, such an approach is not widely used and is generally considered a bad idea <a href="../references.html#vandenbrande-requicha-1993">[Vandenbrande and Requicha, 1993]</a>. Therefore, even when the design features are known from the originating CAD system, they have to be converted to the manufacturing features to generate NC code. Then, more often than not, the design features are not even available because of lossy data exchange between the employed CAD and CAM packages. As a result, feature recognition had become a fundamental problem in the computer-aided design field.
</p>

<p>
  There is a bunch of engineering workflows where feature recognition is highly demanded. Some of them are listed below:
</p>

<ol>
  <li class="text-block"><b>Manufacturability analysis.</b> Before passing a CAD part to a machining shop, one has to be sure that this part can be fabricated in the first place. The manufacturability analysis can be conducted to assess all holes, pockets, slots, fillets and chamfers on a CAD part for accessibility and fitting the manufacturing constraints.</li>

  <li class="text-block"><b>Direct editing.</b> Direct editing is opposed to history-based editing in the sense that direct editing operations (push/pull, face tweaking, etc.) do not rely on the availability of design features somewhere in a feature tree. Instead, direct editing operators perform local feature recognition in the affected area. Knowing local features allows for changing the part's geometry without corrupting its original design intent.</li>

  <li class="text-block"><b>CAD part simplification (defeaturing).</b> Strictly speaking, simplification (or "defeaturing") is yet another type of direct editing. Simplification is often used to prepare design models for numerical simulation or consumption by downstream systems that do not speak the "CAD language" natively (e.g., for gaming engines). To simplify a feature (e.g., to removal a drilled hole), one needs to recognize that feature in the CAD part in the first place.</li>

  <li class="text-block"><b>Manufacturing cost estimation.</b> Digital manufacturing (or "Manufacturing as a Service") is a growing industry. It is often necessary to assess the expected production costs for a CAD part before placing the order. Those costs would naturally include the milling, turning or 3D printing time that highly correlates with the part's features. Therefore, feature recognition is required for accurate machining time estimation.</li>

  <li class="text-block"><b>Manufacturing planning.</b> Even if the design features are known from the originating CAD system (and that is often not the case), automated CNC manufacturing would require specific <i>manufacturing features</i> instead of "form" as-designed features.</li>
</ol>

<p>
  To our knowledge, Analysis Situs is the first, and the only open-sourced CAD software aimed to provide general feature recognition functionality.
</p>

<h2 id="toc-featrec-naive">Why naive approach is not the best?</h2>

<p>
  By "naive" approach we mean the technique of iterating CAD model's faces and grouping them together to compose the features. Such scanning approach is not completely desperate, and, if implemented wisely, it can end up with a workable solution.
</p>

<div align="center"><img src="../imgs/situ_featrec-scanning.gif"/></div>

<p>
  At the same time, such a naive approach is fundamentally "blind" as here we do not employ any formalism to ground our recognition algorithm upon. What we do here is just visiting all the CAD faces and analyzing their analytical properties to deduce if they "look like a feature" or not. The following subsections uncover the principal complexities of the recognition process.
</p>

<!-- <h3 id="toc-featrec-naive-inter">Interacting or generalized features</h3> -->

<p>
  It is relatively easy to extract all plain cylindrical holes out of a CAD model if you are well equipped and know how to program (Python can be just fine, although we prefer C++ here). The things get more laborious if you need to distinguish between plain, countersunk, counterbored and counterdrilled holes.
</p>

<div align="center"><img src="../imgs/situ_featrec-drilled-holes.png"/></div>

<p>
  Looking at the image above, you can imagine a few more combinations of countersinks and counterbores, including variations of blind and through holes. The number of possible feature patterns is quite big even for such a simple thing as a drilled hole. Now imagine that these holes perforate each other:
</p>

<div align="center"><img src="../imgs/situ_featrec-interacting-holes.gif"/></div>

<p>
  Even without interactions, some holes could not be easily classified as they correspond to a sequence of milling operations, such as boring stacked up steps:
</p>

<div align="center"><img src="../imgs/situ_featrec-complex-machined-hole.png"/></div>

<p>
  In the presence of interacting and complex features, simple scanning technique becomes error-prone unless it is equipped with a rich set of topologic and geometric checks all applied following some specific recognition strategy. Elaborating such a strategy is not a trivial exercise, although the heuristics employed down the road might not be that complicated.
</p>

<!-- <h3 id="toc-featrec-naive-ds">Data structures</h3> -->

<p>
  The principal data structure guiding feature recognition is, of course, the boundary representation of a CAD part itself. Since Analysis Situs is based on OpenCascade, we employ its minimalistic B-rep structure that does not support neither user-defined attributes nor convenient methods of iteration (such as back-references and circulators). Therefore, a computationally efficient feature recognition algorithm should employ some extra data structures, such as:
</p>

<ol>
  <li class="text-block">Attributes holder. This data structure is aimed at storing all the geometric cues and "knowledge" about the CAD features as they are being extracted by the recognition algorithms progressively.</li>

  <li class="text-block">Adjacency graphs. These graph data structures allow us to focus on the topological relationships between the boundary elements (and possibly features) and formalize the connectivity of shapes without much care of their geometry.</li>

  <li class="text-block">Iterators. It is critical to have efficient iterators over the B-rep elements to be able to query, for example, all parent faces for the given edge.</li>
</ol>

<p>
  Having a rich B-rep modeller under the hood of a feature recognizer is essential not only because we need to represent a part's shape with all its boundaries. Besides that, we also need to ask various "geometric questions" in the course of recognition. Therefore, we have to leverage the modelling kernel's API to solve the recognition problem.
</p>

<!-- <h3 id="toc-featrec-naive-formalism">Lack of formalism</h3> -->

<p>
  A <i>sound idea</i> is the key ingerient to any geometric algorithm. If a good idea is missing (and that is often the case with naive scan approaches), the algorithm has good chances to become a "big ball of mud" <a href="../references.html#big-ball-of-mood">[Foote and Yoder, 1997]</a>, i.e. a piece of puzzled non-maintainable code.
</p>

<p>
  Many scientific papers and technical reports have been published with the aim to document a well-proven feature recognition method. One of such methods consists in injecting the formalism of graph theory aimed at building up the graph-based abstraction of the recognition problem. The driving idea here consists of converting the geometric representation of a CAD part to a pure topological representation which facilitates computation.
</p>

<h1 id="toc-graphbased">Graph-based feature recognition</h1>

TODO

<h2 id="toc-graphbased-aag">Attributed Adjacency Graph (AAG)</h2>

TODO

<h2 id="toc-graphbased-iso">Finding isomorphous faces</h2>

TODO

<h1 id="toc-integration">Integration notes</h1>

TODO

<h2 id="toc-integration-app">Application</h2>

TODO

<h2 id="toc-integration-sdk">Software Development Kit</h2>

TODO

<h2 id="toc-integration-services">Custom development services</h2>

TODO

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    Copyright &copy; Analysis&nbsp;Situs 2015-present &nbsp; | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://analysis-situs.medium.com/" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

</body>
</html>
