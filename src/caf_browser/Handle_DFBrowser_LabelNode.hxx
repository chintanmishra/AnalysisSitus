/***************************************************************************
 *   Copyright (c) OPEN CASCADE SAS                                        *
 *                                                                         *
 *   This file is part of Open CASCADE Technology software library.        *
 *                                                                         *
 *   This library is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU Library General Public           *
 *   License as published by the Free Software Foundation; either          *
 *   version 2 of the License, or (at your option) any later version.      *
 *                                                                         *
 *   This library  is distributed in the hope that it will be useful,      *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 ***************************************************************************/

#ifndef _Handle_DFBrowser_LabelNode_h
#define _Handle_DFBrowser_LabelNode_h

#include <Standard_DefineHandle.hxx>
#include <Handle_DFBrowser_DFNode.hxx>

class DFBrowser_LabelNode;

DEFINE_STANDARD_HANDLE(DFBrowser_LabelNode,DFBrowser_DFNode)

#endif
